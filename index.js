import {HangmanPresenter} from "./hangman/hangman_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new HangmanPresenter();
        }
    }
}

Starter.start();
