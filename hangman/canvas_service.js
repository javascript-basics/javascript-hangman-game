const CANVAS_CLASS = 'hangman-canvas';
const COLOR_VALUE = '#1d3557';

export class CanvasView {
    #canvasElement;
    #context;

    constructor() {
        this.#init();
    }

    #init() {
        this.#canvasElement = (document.getElementsByClassName(CANVAS_CLASS) || document.createDocumentFragment().children).item(0);
        this.#context = (this.#canvasElement || document.createElement("canvas")).getContext('2d');
        this.setContextStrokeStyle(COLOR_VALUE);
    }

    setContextStrokeStyle(color) {
        this.#context.strokeStyle = color;
    }

    drawLine(startX, startY, endX, endY) {
        this.#context.beginPath();

        this.#context.moveTo(startX, startY);
        this.#context.lineTo(endX, endY);
        this.#context.stroke();

        this.#context.closePath();
    }

    drawCircle(x, y, radius) {
        this.#context.beginPath();
        const fullCircleAngle = Math.PI * 2;

        this.#context.arc(x, y, radius, 0, fullCircleAngle);
        this.#context.stroke();
    }

    resetCanvas() {
        this.#context.clearRect(0, 0, this.#canvasElement.width, this.#canvasElement.height);
    }
}
