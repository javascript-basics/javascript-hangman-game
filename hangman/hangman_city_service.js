import {HttpRequest} from "./http_request.js";

function getRandomOffset() {
    return Math.floor(Math.random() * (FREE_ACCOUNT_MAX_OFFSET_VALUE + 1));
}

const FREE_ACCOUNT_MAX_OFFSET_VALUE = 23779;
const API = 'http://geodb-free-service.wirefreethought.com//v1/geo/cities';
const CITY_LIMIT = 1;

export class HangmanCityService {
    #cityPromise;
    #httpRequest = new HttpRequest();

    constructor() {
        this.getNewCity();
    }

    get cityPromise() {
        return this.#cityPromise;
    }

    getNewCity() {
        this.#cityPromise = new Promise(this.#handlePromise.bind(this));
    }

    #handlePromise(resolve) {
        const offset = getRandomOffset();
        const url = `${API}?limit=${CITY_LIMIT}&offset=${offset}`;

        this.#httpRequest.createGetRequest(url, this.#handleResponse.bind(this, resolve));
    }

    #handleResponse(resolve, progressEvent) {
        const response = JSON.parse(((progressEvent || new ProgressEvent('loadend')).target ||
            new XMLHttpRequest()).responseText);

        const responseObj = (response.data || [])[0];

        const city = (responseObj || {city: ''}).city;
        const onlyLetterRegExp = new RegExp(/[^a-zA-Z\s]/);

        const correctValue = city.length <= 20 && !onlyLetterRegExp.test(city);

        if (!correctValue) {
            this.#handlePromise(resolve);
            return;
        }

        resolve(city.toLowerCase());
    }
}
