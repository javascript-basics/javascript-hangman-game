function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}

const LETTER_CLASS = 'letter';
const HIDDEN_LETTER_CLASS = 'hidden-letter';
const HIDDEN_WHITESPACE_CLASS = `${HIDDEN_LETTER_CLASS}-whitespace`;

const STATUS_CLASS = 'status';
const LIFE_COUNT_ELEMENT = 'life-value';
const CITY_SECTION_CLASS = 'hidden-city-section';

const STRIKE_THROUGH_CLASS = 'strike-through';

const PLAY_AGAIN_BTN_CLASS = 'play-again-btn';

const COLOR_VALUE = '#1d3557';
const WHITESPACE_VALUE = ' ';

export class HangmanView {
    #statusElement = Object.create(HTMLElement.prototype, {});
    #lifeCountElement = Object.create(HTMLElement.prototype, {});
    #citySection = Object.create(HTMLElement.prototype, {});

    get playAgainBtn() {
        return getElementByClassName(PLAY_AGAIN_BTN_CLASS);
    }

    get letters() {
        return elementGenerator(LETTER_CLASS);
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#statusElement = getElementByClassName(STATUS_CLASS);
        this.#lifeCountElement = getElementByClassName(LIFE_COUNT_ELEMENT);
        this.#citySection = getElementByClassName(CITY_SECTION_CLASS);
    }

    updateStatusContent(content) {
        this.#statusElement.innerText = content;
    }

    updateLifeCountContent(content) {
        this.#lifeCountElement.innerText = content;
    }

    hasStrikeThroughToLetterClass(value) {
        return (this.getLetterByValue(value) || new HTMLElement()).classList.contains(STRIKE_THROUGH_CLASS);
    }

    getLetterByValue(value) {
        const className = `${LETTER_CLASS}-${value}`;
        return getElementByClassName(className);
    }

    getHiddenLettersByValue(value) {
        const className = `${HIDDEN_LETTER_CLASS}-${value}`;
        return [...document.getElementsByClassName(className) || document.createDocumentFragment().children];
    }

    addStrikeThroughToLetter(letter) {
        (letter || new HTMLElement()).classList.add(STRIKE_THROUGH_CLASS);
    }

    removeStrikeThroughToLetter(letter) {
        (letter || new HTMLElement()).classList.remove(STRIKE_THROUGH_CLASS);
    }

    addColorToLetter(letter) {
        (letter || new HTMLElement()).style.color = COLOR_VALUE;
    }

    createLetter(letter) {
        const letterElement = document.createElement('span');

        const className = letter === WHITESPACE_VALUE ? HIDDEN_WHITESPACE_CLASS : `${HIDDEN_LETTER_CLASS}-${letter}`;

        letterElement.innerText = letter;
        letterElement.classList.add(HIDDEN_LETTER_CLASS, className);
        return letterElement;
    }

    resetCitySection() {
        this.#citySection.innerHTML = null;
    }

    appendToCitySection(element) {
        this.#citySection.appendChild(element);
    }
}
