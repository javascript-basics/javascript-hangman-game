import {HangmanCityService} from "./hangman_city_service.js";

const EMPTY_CITY = '';
const ZERO_LETTERS = 0;
const LOSE_CONDITION_VALUE = 0;

const DEFAULT_LIFE_VALUE = 10;
const WHITESPACE_VALUE = ' ';

const DEFAULT_STATUS = 'Use the alphabet below to guess the city, or type the letter using the keyboard';
const RIGHT_LETTER_STATUS = 'You guess';
const WRONG_LETTER_STATUS = 'Wrong letter! Try again';
const WIN_STATUS = 'You win! Try again?';
const LOSE_STATUS = 'You lose';

export class HangmanService {
    #presenter;
    #cityService = new HangmanCityService();

    #hiddenCity = EMPTY_CITY;
    #lettersLeft = ZERO_LETTERS;

    #lifeValue = DEFAULT_LIFE_VALUE;
    #currentStatus = DEFAULT_STATUS;

    get currentStatus() {
        return this.#currentStatus;
    }

    get lifeValueStr() {
        return `You have ${this.#lifeValue} lives`;
    }

    get gameEnded() {
        return this.#lettersLeft === ZERO_LETTERS || this.#lifeValue === LOSE_CONDITION_VALUE;
    }

    constructor(presenter) {
        this.#presenter = presenter;
        this.#init();
    }

    #init() {
        this.#getCity().then(this.#initHiddenCity.bind(this));
    }

    reset() {
        this.#currentStatus = DEFAULT_STATUS;
        this.#lifeValue = DEFAULT_LIFE_VALUE;

        this.#cityService.getNewCity();
        this.#getCity().then(this.#initHiddenCity.bind(this));
    }

    async #getCity() {
        return await this.#cityService.cityPromise;
    }

    #initHiddenCity(city) {
        this.#hiddenCity = city;

        this.#lettersLeft = this.#hiddenCity.split('')
            .filter((value, index, self) => self.indexOf(value) === index && value !==
                WHITESPACE_VALUE).length;

        this.#presenter.init(this.#hiddenCity);
    }

    handleLetter(letter) {
        if (this.#hiddenCity.includes(letter)) {
            this.#handleRightLetter(letter);
        } else {
            this.#handleWrongLetter();
        }
    }

    #handleRightLetter(letter) {
        this.#presenter.showRightLetters(letter);
        this.#lettersLeft--;
        this.#currentStatus = this.#lettersLeft === LOSE_CONDITION_VALUE ? WIN_STATUS :
            `${RIGHT_LETTER_STATUS} "${letter}"`;
    }

    #handleWrongLetter() {
        this.#presenter.drawHangman(this.#lifeValue);
        this.#lifeValue--;
        this.#currentStatus = this.#lifeValue === LOSE_CONDITION_VALUE ? LOSE_STATUS : WRONG_LETTER_STATUS;
    }
}
