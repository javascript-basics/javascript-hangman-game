export class HttpRequest {
    createGetRequest(url, callback) {
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.onload = callback;
        xmlHttp.open('GET', url, true);
        xmlHttp.send();
    }
}
