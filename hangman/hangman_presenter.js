import {HangmanService} from "./hangman_service.js";
import {HangmanView} from "./hangman_view.js";
import {CanvasView} from "./canvas_service.js";

const CANVAS_ARGS = [
    [10, 140, 290, 140],
    [40, 140, 40, 10],
    [30, 10, 240, 10],
    [160, 10, 160, 40],
    [160, 52, 12],
    [160, 64, 160, 100],
    [160, 70, 145, 90],
    [160, 70, 175, 90],
    [160, 100, 145, 125],
    [160, 100, 175, 125]
];

const DRAW_CIRCLE_INDEX = 6;

export class HangmanPresenter {
    #hangmanService = new HangmanService(this);
    #hangmanView = new HangmanView();
    #canvasService = new CanvasView();

    #canvasMapper = new Map();

    #keydownHandler;

    init(city) {
        this.#initCanvasMapper();

        this.#removeListeners();
        this.#removePlayAgainBtnListener();

        this.#hangmanView.resetCitySection();
        this.#hangmanView.updateStatusContent(this.#hangmanService.currentStatus);

        this.#keydownHandler = event => this.#checkKey((event || new KeyboardEvent('keydown')).key.toLowerCase());
        this.#initListeners();

        (city || '').split('').forEach(letter => {
            const letterElement = this.#hangmanView.createLetter(letter);
            this.#hangmanView.appendToCitySection(letterElement);
        });
    }

    #initCanvasMapper() {
        const size = CANVAS_ARGS.length;

        for (let i = size; i > 0; i--) {
            if (i === DRAW_CIRCLE_INDEX) {
                this.#canvasMapper.set(i, () => this.#canvasService.drawCircle(...CANVAS_ARGS[size - i]));
                continue;
            }

            this.#canvasMapper.set(i, () => this.#canvasService.drawLine(...CANVAS_ARGS[size - i]));
        }
    }

    #initListeners() {
        document.addEventListener('keydown', this.#keydownHandler);

        const playAgainBtn = this.#hangmanView.playAgainBtn;
        (playAgainBtn || new HTMLElement()).addEventListener('click', this.#handlePlayAgainBtn.bind(this));

        this.#initLettersListeners();
    }

    #handlePlayAgainBtn() {
        this.#removeStrikesFromLetters();
        this.#canvasService.resetCanvas();

        this.#hangmanService.reset();

        this.#hangmanView.updateStatusContent(this.#hangmanService.currentStatus);
        this.#hangmanView.updateLifeCountContent(this.#hangmanService.lifeValueStr);
    }

    #initLettersListeners() {
        const letters = this.#hangmanView.letters;
        const handler = event => this.#checkKey(((event || new Event('click')).target || new HTMLElement())
            .textContent.toLowerCase());

        for (let letter of letters) {
            (letter || new HTMLElement()).addEventListener('click', handler.bind(this));
        }
    }

    #checkKey(key) {
        if (this.#hangmanView.hasStrikeThroughToLetterClass(key)) {
            return;
        }

        const isChar = key >= 'a' && key <= 'z';

        if (isChar) {
            this.#handleLetter(key);
        }
    }

    #handleLetter(letter) {
        this.#hangmanService.handleLetter(letter);
        this.#hangmanView.updateStatusContent(this.#hangmanService.currentStatus);
        this.#hangmanView.updateLifeCountContent(this.#hangmanService.lifeValueStr);

        if (this.#hangmanService.gameEnded) {
            this.#removeListeners();
            return;
        }

        const letterElement = this.#hangmanView.getLetterByValue(letter);

        this.#hangmanView.addStrikeThroughToLetter(letterElement);
        const clone = (letterElement || new HTMLElement()).cloneNode(true);
        (letterElement || new HTMLElement()).parentNode.replaceChild(clone, letterElement);
    }

    #removeListeners() {
        document.removeEventListener('keydown', this.#keydownHandler);
        this.#removeLettersListeners();
    }

    #removeLettersListeners() {
        const letters = this.#hangmanView.letters;

        for (let letter of letters) {
            const clone = (letter || new HTMLElement()).cloneNode(true);
            (letter || new HTMLElement()).parentNode.replaceChild(clone, letter);
        }
    }

    #removePlayAgainBtnListener() {
        const playAgainBtn = this.#hangmanView.playAgainBtn;
        const clone = (playAgainBtn || new HTMLElement()).cloneNode(true);
        (playAgainBtn || new HTMLElement()).parentNode.replaceChild(clone, playAgainBtn);
    }

    #removeStrikesFromLetters() {
        const letters = this.#hangmanView.letters;

        for (let letter of letters) {
            this.#hangmanView.removeStrikeThroughToLetter(letter);
        }
    }

    showRightLetters(letter) {
        const letterElements = this.#hangmanView.getHiddenLettersByValue(letter);

        for (let letterElement of letterElements) {
            this.#hangmanView.addColorToLetter(letterElement);
        }
    }

    drawHangman(lifeValue) {
        this.#canvasMapper.get(lifeValue)();
    }
}
